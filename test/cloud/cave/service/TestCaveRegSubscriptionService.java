package cloud.cave.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import cloud.cave.domain.Login;
import cloud.cave.domain.LoginResult;
import cloud.cave.doubles.TestStubCaveRegSubscriptionService;
import cloud.cave.server.common.SubscriptionResult;
import org.junit.*;

import cloud.cave.config.CaveServerFactory;
import cloud.cave.domain.Cave;
import cloud.cave.doubles.SubscriptionTestDoubleFactory;
import cloud.cave.server.StandardServerCave;
import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.server.common.SubscriptionRecord;

public class TestCaveRegSubscriptionService {
    private Cave cave = null;
    private SubscriptionService cavereg;
    private String loginName = "20117411";
    private String pwd = "1337boi";


    @Before
    public void setUp() {
        cavereg = new TestStubCaveRegSubscriptionService();
        cavereg.initialize(new ServerConfiguration("cavereg.baerbak.com", 4567));

        CaveServerFactory factory = new SubscriptionTestDoubleFactory();
        cave = new StandardServerCave(factory);
    }

    @Test
    public void shouldReturnValidSubscriptionRecord() {
        SubscriptionRecord record = cavereg.lookup(loginName, pwd);
        assertThat(record.getErrorCode(), is(SubscriptionResult.LOGIN_NAME_HAS_VALID_SUBSCRIPTION));
    }

    @Test
    public void shouldReturnInvalidSubscriptionRecord() {
        SubscriptionRecord record = cavereg.lookup(loginName + "1", pwd); //wrong loginName
        assertThat(record.getErrorCode(), is(SubscriptionResult.LOGIN_NAME_OR_PASSWORD_IS_UNKNOWN));
    }

    @Test
    public void shouldSucceedInLogin() {
        Login loginResult = cave.login(loginName, pwd);
        assertThat(loginResult.getResultCode(), is(LoginResult.LOGIN_SUCCESS));
    }

    @Test
    public void shouldFailInLogin() {
        Login loginResult = cave.login(loginName + "1", pwd); //wrong loginName
        assertThat(loginResult.getResultCode(), is(LoginResult.LOGIN_FAILED_UNKNOWN_SUBSCRIPTION));
    }

    @Test
    public void shouldReturnServerConfiguration() {
        ServerConfiguration configuration = cavereg.getConfiguration();
        assertThat(configuration.toString(), containsString("cavereg.baerbak.com:4567"));
    }

}
