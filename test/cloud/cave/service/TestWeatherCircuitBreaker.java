package cloud.cave.service;

import cloud.cave.domain.Region;
import cloud.cave.doubles.TestStubWeatherCircuitBreaker;
import org.json.simple.JSONObject;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class TestWeatherCircuitBreaker {

    CaveWeatherCircuitBreaker weatherService;
    TestStubWeatherCircuitBreaker testStub;
    JSONObject response;

    @Before
    public void setUp(){
        testStub = new TestStubWeatherCircuitBreaker();
        weatherService = new CaveWeatherCircuitBreaker();

        weatherService.setWeatherService(testStub);
    }

    @Test
    public void shouldBeInStateClosed(){
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        assertThat(response.get("errorMessage").toString(), is("OK"));
    }

    @Test
    public void shouldGoToOpenState() {
        testStub.throwException(true);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);

        assertThat(response.get("errorMessage").toString(),
                is("*** Sorry - weather information is not available ***"));

        CaveWeatherCircuitBreaker.State state = weatherService.getState();
        assertThat(state, is(CaveWeatherCircuitBreaker.State.OPEN));
    }

    @Test
    public void shouldBeInOpenState() {
        testStub.throwException(true);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);

        assertThat(response.get("errorMessage").toString(),
                is("*** Sorry - no weather (open circuit) ***"));

        CaveWeatherCircuitBreaker.State state = weatherService.getState();
        assertThat(state, is(CaveWeatherCircuitBreaker.State.OPEN));
    }

    @Test
    public void shouldGoFromHalfOpenToClosed() {
        testStub.throwException(true);

        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);

        weatherService.setTimerThreshold(0);
        testStub.throwException(false);

        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);

        assertThat(response.get("errorMessage").toString(), is("OK"));
    }

    @Test
    public void shouldGoFromHalfOpenToOpen() {
        testStub.throwException(true);

        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);
        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);

        weatherService.setTimerThreshold(0);

        response = weatherService.requestWeather("ITS", "201205397", Region.ODENSE);

        CaveWeatherCircuitBreaker.State state = weatherService.getState();
        assertThat(state, is(CaveWeatherCircuitBreaker.State.OPEN));
    }
}
