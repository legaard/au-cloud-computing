package cloud.cave.service;

import cloud.cave.domain.Region;
import cloud.cave.ipc.CaveIPCException;
import cloud.cave.server.common.PlayerRecord;
import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.server.common.SubscriptionRecord;
import cloud.cave.server.common.SubscriptionResult;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongoConfig;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TestSubscriptionServiceAvailabilityDecorator {
    private final String loginName = "20117411";
    private final String password = "1337boi";
    private SubscriptionService subscriptionService;

    private MongodProcess mongodProcess;
    private MongoDBStorage storage;
    private int port = 12345;

    @Before
    public void setUp() {
        subscriptionService = new SubscriptionServiceAvailabilityDecorator();

        MongodStarter starter = MongodStarter.getDefaultInstance();

        try {
            IMongoConfig mongoConfig = new MongodConfigBuilder()
                    .version(Version.Main.PRODUCTION)
                    .net(new Net(port, Network.localhostIsIPv6()))
                    .build();

            MongodExecutable mongodExecutable = null;
            mongodExecutable = starter.prepare((IMongodConfig) mongoConfig);

            mongodProcess = mongodExecutable.start();

            storage = new MongoDBStorage();
            storage.initialize(new ServerConfiguration("localhost", port));

        } catch (IOException e) {
            e.printStackTrace();
        }

        // Initialize subscription service with correct host and port
        subscriptionService.initialize(new ServerConfiguration("cavereg.baerbak.com", 4567, "localhost", port));
    }

    @After
    public void shutdown(){
        if(mongodProcess != null){
            mongodProcess.stop();
        }
    }

    @Ignore
    @Test
    public void shouldReturnValidSubscriptionRecord() {
        SubscriptionRecord record = subscriptionService.lookup(loginName, password);
        // Should contact online subscription service and provide valid SubscriptionResult
        assertThat(record.getErrorCode(), is(SubscriptionResult.LOGIN_NAME_HAS_VALID_SUBSCRIPTION));
    }

    @Test(expected = CaveIPCException.class)
    public void shouldThrowCaveIPCException() {
        // Set wrong port for online subscription service in order to get exception
        subscriptionService.initialize(new ServerConfiguration("cavereg.baerbak.com", 4568, "localhost", port));
        SubscriptionRecord record = subscriptionService.lookup(loginName, password);
        assertThat(record.getErrorCode(), is(SubscriptionResult.LOGIN_NAME_OR_PASSWORD_IS_UNKNOWN));
    }

    @Test
    public void shouldLoginKnownUser() {
        // Create user information in DB to simulate an already known user
        SubscriptionRecord subscriptionRecord = new SubscriptionRecord("55e40a04e4b067dd3c8fa51f", "Lorentzen", "ITS 13", Region.AARHUS);
        PlayerRecord playerRecord = new PlayerRecord(subscriptionRecord, "(0,0,0)", null);
        storage.updatePlayerRecord(playerRecord);

        // Connect to local mongoDB to insert known user data
        MongoClient mongoClient = new MongoClient("localhost", port);
        MongoDatabase database = mongoClient.getDatabase(DBConstants.DATABASE_NAME);
        MongoCollection<Document> knownUsers = database.getCollection(DBConstants.KNOWN_USERS);

        Document newKnownUser = new Document(DBConstants.KNOWN_USERNAME, loginName)
                .append(DBConstants.PLAYER_ID, subscriptionRecord.getPlayerID())
                .append(DBConstants.KNOWN_USER_PASSWORD, password);
        knownUsers.insertOne(newKnownUser);

        // Initialize subscription service with wrong port in order to get exception
        subscriptionService.initialize(new ServerConfiguration("cavereg.baerbak.com", 4568, "localhost", port));
        SubscriptionRecord record = subscriptionService.lookup(loginName, password);
        assertThat(record.getErrorCode(), is(SubscriptionResult.LOGIN_NAME_HAS_VALID_SUBSCRIPTION));
    }



}
