package cloud.cave.service;

import cloud.cave.config.CaveServerFactory;
import cloud.cave.domain.Cave;
import cloud.cave.domain.Player;
import cloud.cave.domain.Region;
import cloud.cave.doubles.WeatherServiceTestDoubleFactory;
import cloud.cave.server.StandardServerCave;
import cloud.cave.server.common.ServerConfiguration;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class TestCaveWeatherService {
    private WeatherService weatherService;
    private JSONObject result;
    private String groupName = "ITS13";
    private String playerID = "55e40a04e4b067dd3c8fa51f";

    @Ignore
    @Before
    public void setUp() {
        weatherService = new CaveWeatherService();
        weatherService.initialize(new ServerConfiguration("caveweather.baerbak.com", 8182));
        result = new JSONObject();
    }
    @Ignore
    @Test
    public void shouldReturnValidWeatherInformationForAarhus() {
        result = weatherService.requestWeather(groupName, playerID, Region.AARHUS);
        assertThat(result.get("errorMessage").toString(), is("OK"));
    }
    @Ignore
    @Test
    public void shouldReturnValidWeatherInformationForCopenhagen() {
        result = weatherService.requestWeather(groupName, playerID, Region.COPENHAGEN);
        assertThat(result.get("errorMessage").toString(), is("OK"));
    }
    @Ignore
    @Test
    public void shouldReturnValidWeatherInformationForAalborg() {
        result = weatherService.requestWeather(groupName, playerID, Region.AALBORG);
        assertThat(result.get("errorMessage").toString(), is("OK"));
    }
    @Ignore
    @Test
    public void shouldReturnInvalidWeatherInformation() {
        result = weatherService.requestWeather(groupName, playerID, Region.NONE);
        assertThat(result.get("errorMessage").toString(), not("OK"));
    }
    @Ignore
    @Test
    public void shouldReturnWeatherInOdense(){
        CaveServerFactory factory = new WeatherServiceTestDoubleFactory();
        Cave cave = new StandardServerCave(factory);
        Player player = cave.login("201205397", "52773056").getPlayer();

        String result = player.getWeather();
        assertThat(result, containsString("ODENSE"));
    }
    @Ignore
    @Test
    public void shouldReturnServerConfiguration(){
        ServerConfiguration configuration = weatherService.getConfiguration();
        assertThat(configuration.toString(), containsString("caveweather.baerbak.com:8182"));
    }

 }
