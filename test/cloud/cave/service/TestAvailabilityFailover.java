package cloud.cave.service;

import cloud.cave.domain.Login;
import cloud.cave.domain.Player;
import cloud.cave.doubles.MongoDBStorageSaboteur;
import cloud.cave.doubles.StorageTestDoubleFactory;
import cloud.cave.ipc.MarshalingKeys;
import cloud.cave.server.PlayerDispatcher;
import cloud.cave.server.StandardServerCave;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongoConfig;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TestAvailabilityFailover {

    PlayerDispatcher playerDispatcher;
    Player player;
    JSONObject response;
    int port = 12345;
    MongodProcess mongodProcess;

    @Before
    public void setUp(){

        MongodStarter mongodStarter = MongodStarter.getDefaultInstance();

        try {
            IMongoConfig mongoConfig = new MongodConfigBuilder()
                    .version(Version.Main.PRODUCTION)
                    .net(new Net(port, Network.localhostIsIPv6()))
                    .build();

            MongodExecutable mongodExecutable = null;
            mongodExecutable = mongodStarter.prepare((IMongodConfig) mongoConfig);
            mongodProcess = mongodExecutable.start();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @After
    public void shutdown(){
        if(mongodProcess != null){
            mongodProcess.stop();
        }
    }

    public void setupExceptionType(MongoDBStorageSaboteur.MongoExceptionType exceptionType){

        //Create a fake storage that uses
        StorageTestDoubleFactory factory = new StorageTestDoubleFactory();
        factory.setExceptionType(exceptionType);
        StandardServerCave standardServerCave = new StandardServerCave(factory);
        Login loginResult = standardServerCave.login("mikkel_aarskort", "123");

        //Instantiate a player dispatcher
        playerDispatcher = new PlayerDispatcher(standardServerCave);
        player = loginResult.getPlayer();

    }


    @Test
    public void shouldCatchMongoTimeOutException() {
        setupExceptionType(MongoDBStorageSaboteur.MongoExceptionType.TIMEOUT);
        JSONObject object = new JSONObject();
        object.put("Testing", "Awesomeness");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(object);
        response = playerDispatcher.dispatch(MarshalingKeys.DIG_ROOM_METHOD_KEY, player.getID(),
                player.getSessionID(), "UP", jsonArray);
        assertThat(response.get("error-message").toString(), is("Server Storage Error!"));
    }

    @Test
    public void shouldCatchMongoSocketReadException(){
        setupExceptionType(MongoDBStorageSaboteur.MongoExceptionType.SOCKET_READ);
        JSONObject object = new JSONObject();
        object.put("Testing", "Awesomeness");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(object);
        response = playerDispatcher.dispatch(MarshalingKeys.DIG_ROOM_METHOD_KEY, player.getID(),
                player.getSessionID(), "UP", jsonArray);
        assertThat(response.get("error-message").toString(), is("Server Storage Error!"));
    }

    @Test
    public void shouldCatchMongoSocketClosedException(){
        setupExceptionType(MongoDBStorageSaboteur.MongoExceptionType.SOCKET_CLOSED);
        JSONObject object = new JSONObject();
        object.put("Testing", "Awesomeness");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(object);
        response = playerDispatcher.dispatch(MarshalingKeys.DIG_ROOM_METHOD_KEY, player.getID(),
                player.getSessionID(), "UP", jsonArray);
        assertThat(response.get("error-message").toString(), is("Server Storage Error!"));
    }

    @Test
    public void shouldCatchMongoQueryException(){
        setupExceptionType(MongoDBStorageSaboteur.MongoExceptionType.QUERY);
        JSONObject object = new JSONObject();
        object.put("Testing", "Awesomeness");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(object);
        response = playerDispatcher.dispatch(MarshalingKeys.DIG_ROOM_METHOD_KEY, player.getID(),
                player.getSessionID(), "UP", jsonArray);
        assertThat(response.get("error-message").toString(), is("Server Storage Error!"));
    }


}
