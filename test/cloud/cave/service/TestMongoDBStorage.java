package cloud.cave.service;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import cloud.cave.domain.Direction;
import cloud.cave.domain.Region;
import cloud.cave.server.common.*;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongoConfig;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestMongoDBStorage {

    MongoDBStorage storage;
    MongodProcess mongodProcess;

    @Before
    public void setUp(){
        MongodStarter starter = MongodStarter.getDefaultInstance();

        int port = 12345;

        try {
            IMongoConfig mongoConfig = new MongodConfigBuilder()
                    .version(Version.Main.PRODUCTION)
                    .net(new Net(port, Network.localhostIsIPv6()))
                    .build();

            MongodExecutable mongodExecutable = null;
            mongodExecutable = starter.prepare((IMongodConfig) mongoConfig);

            mongodProcess = mongodExecutable.start();

            storage = new MongoDBStorage();
            storage.initialize(new ServerConfiguration("localhost", port));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void shutdown(){
        if(mongodProcess != null){
            mongodProcess.stop();
        }
    }

    @Test
    public void shouldReturnRoomDescriptionOfPosition000() {
        RoomRecord roomRecord = storage.getRoom("(0,0,0)");
        assertThat(roomRecord.description, is("You are standing at the end of a road before a small brick building."));
    }

    @Test
    public void theGetRoomMethodShouldReturnNullIfNoRoomExists(){
        RoomRecord roomRecord = storage.getRoom("(0,0,-1)");
        assertThat(roomRecord, is(nullValue()));
    }

    @Test
    public void shouldReturnListOfValidExitsForPositions000And010() {
        List<Direction> listOfExists = new ArrayList<>();

        //Adding directions for positions (0,0,0)
        listOfExists.add(Direction.NORTH);
        listOfExists.add(Direction.EAST);
        listOfExists.add(Direction.WEST);
        listOfExists.add(Direction.UP);

        assertThat(listOfExists, is(storage.getSetOfExitsFromRoom("(0,0,0)")));

        //Clear list and add directions for positions (0,1,0)
        listOfExists.clear();
        listOfExists.add(Direction.SOUTH);

        assertThat(listOfExists, is(storage.getSetOfExitsFromRoom("(0,1,0)")));
    }

    @Test
    public void shouldUpdatePlayerRecord(){
        SubscriptionRecord newSubscriptionRecord = new SubscriptionRecord("1337", "Legaard", "Group 15", Region.ODENSE);
        PlayerRecord newPlayer = new PlayerRecord(newSubscriptionRecord, "(0,0,0)", "invalid");

        storage.updatePlayerRecord(newPlayer);

        assertThat(storage.getPlayerByID("1337").getSessionId(), is("invalid"));
    }

    @Test
    public void shouldComputeListAndCountOfValidPlayers(){
        SubscriptionRecord invalidSubscriptionRecord = new SubscriptionRecord("invalidPlayer", "Lorentzen", "Group 15", Region.AARHUS);
        PlayerRecord invalidPlayer = new PlayerRecord(invalidSubscriptionRecord, "(0,0,0)", null);

        SubscriptionRecord subscriptionRecord2 = new SubscriptionRecord("razboi", "Lorentzen", "Group 15", Region.AARHUS);
        PlayerRecord newPlayer = new PlayerRecord(subscriptionRecord2, "(0,0,0)", "valid");

        SubscriptionRecord newSubscriptionRecord = new SubscriptionRecord("1337", "Legaard", "Group 15", Region.ODENSE);
        PlayerRecord playerRecord = new PlayerRecord(newSubscriptionRecord, "(0,0,0)", "valid");

        storage.updatePlayerRecord(playerRecord);
        storage.updatePlayerRecord(invalidPlayer);
        storage.updatePlayerRecord(newPlayer);

        List<PlayerRecord> listOfValidPlayers = new ArrayList<>();
        listOfValidPlayers.add(playerRecord);
        listOfValidPlayers.add(newPlayer);

        assertThat(storage.computeListOfPlayersAt("(0,0,0)"), is(listOfValidPlayers));
        assertThat(storage.computeCountOfActivePlayers(), is(2));

    }

    @Test
    public void shouldOutputFirst10Players() {
        for (int i = 0; i < 100; i++) {
            SubscriptionRecord subscriptionRecord = new SubscriptionRecord("100" + i, "Player" + i, "ITS 13", Region.AARHUS);
            PlayerRecord playerRecord = new PlayerRecord(subscriptionRecord, "(0,0,0)", "valid");
            storage.updatePlayerRecord(playerRecord);
        }

        List<PlayerRecord> playerList = storage.computeListOfPlayersAt("(0,0,0)");
        assertThat(playerList.get(0).getPlayerName(), is("Player0"));
        assertThat(playerList.get(9).getPlayerName(), is("Player9"));
    }

    @Test
    public void shouldOutputNext20Players() {
        for (int i = 0; i < 100; i++) {
            SubscriptionRecord subscriptionRecord = new SubscriptionRecord("100" + i, "Player" + i, "ITS 13", Region.AARHUS);
            PlayerRecord playerRecord = new PlayerRecord(subscriptionRecord, "(0,0,0)", "valid");
            storage.updatePlayerRecord(playerRecord);
        }

        List<PlayerRecord> first10Players = storage.computeListOfPlayersAt("(0,0,0)");
        List<PlayerRecord> next20Players = storage.computeListOfPlayersAt("(0,0,0)");
        assertThat(next20Players.get(0).getPlayerName(), is("Player10"));
        assertThat(next20Players.get(19).getPlayerName(), is("Player29"));
    }

}
