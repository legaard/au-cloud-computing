package cloud.cave.ipc;

public class CaveTimeOutException extends CaveIPCException{
    public CaveTimeOutException(String message, Exception originalException) {
        super(message, originalException);
    }
}
