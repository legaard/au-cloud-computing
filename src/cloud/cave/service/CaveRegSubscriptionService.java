package cloud.cave.service;

import cloud.cave.domain.Region;
import cloud.cave.ipc.CaveIPCException;
import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.server.common.SubscriptionRecord;
import cloud.cave.server.common.SubscriptionResult;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

public class CaveRegSubscriptionService implements SubscriptionService{

    private ServerConfiguration configuration;

    @Override
    public SubscriptionRecord lookup(String loginName, String password) {

        JSONObject response = makeSubscriptionRequest(loginName, password);

        if(response == null || !response.getBoolean("success")){
            return new SubscriptionRecord(SubscriptionResult.LOGIN_NAME_OR_PASSWORD_IS_UNKNOWN);
        }

        // Gets the values of the subscribed player
        JSONObject userInfo = response.getJSONObject("subscription");
        SubscriptionRecord subscriptionRecord = null;

        if (userInfo != null){
        String playerID = userInfo.getString("playerID");
        String playerName = userInfo.getString("playerName");
        String groupName = userInfo.getString("groupName");
        String stringRegion = userInfo.getString("region");
        Region region;

            switch (stringRegion) {
                case "AARHUS":
                    region = Region.AARHUS;
                    break;
                case "COPENHAGEN":
                    region = Region.COPENHAGEN;
                    break;
                case "ODENSE":
                    region = Region.ODENSE;
                    break;
                case "AALBORG":
                    region = Region.AALBORG;
                    break;
                default:
                    region = null;
            }

            subscriptionRecord = new SubscriptionRecord(playerID, playerName, groupName, region);
        }

        return subscriptionRecord;
    }


    /**
     * This method makes a http request to the subscription service
     * @param loginName the cave user's login name
     * @param password the cave user's password
     * @return returns null if the request fails otherwise return a JSON object
     */
    private JSONObject makeSubscriptionRequest(String loginName, String password){

        JSONObject response = null;

        try {
            HttpResponse<JsonNode> request = Unirest.get("http://{hostName}:{port}/api/v1/auth?loginName={loginName}&password={password}")
                    .header("accept", "application/json")
                    .routeParam("hostName", configuration.get(0).getHostName())
                    .routeParam("port", String.valueOf(configuration.get(0).getPortNumber()))
                    .routeParam("loginName", loginName)
                    .routeParam("password", password)
                    .asJson();
            response = request.getBody().getObject();

        } catch (UnirestException e) {
            throw new CaveIPCException("*** Sorry - Failed to login, please try again later", e);
        }

        return response;
    }

    @Override
    public void initialize(ServerConfiguration config) {
        configuration = config;
    }

    @Override
    public void disconnect() {

    }

    @Override
    public ServerConfiguration getConfiguration() {
        return configuration;
    }
}
