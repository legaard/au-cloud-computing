package cloud.cave.service;

import cloud.cave.domain.Region;
import cloud.cave.ipc.CaveTimeOutException;
import cloud.cave.server.common.ServerConfiguration;
import org.slf4j.*;
import org.json.simple.JSONObject;

public class CaveWeatherCircuitBreaker implements WeatherService {

    enum State{
        CLOSED, OPEN, HALF_OPEN
    }

    private Logger logger = LoggerFactory.getLogger(CaveWeatherCircuitBreaker.class);

    private WeatherService weatherService = new CaveWeatherService();
    private State state = State.CLOSED;
    private int counter = 0;
    private long timer = 0;
    private int timerThreshold = 20000;

    @Override
    public JSONObject requestWeather(String groupName, String playerID, Region region) {
        JSONObject response = new JSONObject();

        try {

            if(state.equals(State.CLOSED)){
                System.out.println("Circuitbreaker state: " + state);
                logger.info("Circuitbreaker state: " + state);

                counter++;
                response = weatherService.requestWeather(groupName, playerID, region);
            }

            if (state.equals(State.OPEN)){
                System.out.println("Circuitbreaker state: " + state);
                logger.info("Circuitbreaker state: " + state);

                response.put("authenticated", "false");
                response.put("errorMessage", "*** Sorry - no weather (open circuit) ***");

                if(System.currentTimeMillis() - timer >= timerThreshold){
                    state = State.HALF_OPEN;
                }

            }

            if (state.equals(State.HALF_OPEN)){
                System.out.println("Circuitbreaker state: " + state);
                logger.info("Circuitbreaker state: " + state);

                response = weatherService.requestWeather(groupName, playerID, region);

                if(response.get("errorMessage").toString().matches("OK")){
                    state = State.CLOSED;
                }
            }

        } catch (CaveTimeOutException e){
            response.put("authenticated", "false");
            response.put("errorMessage", "*** Sorry - weather information is not available ***");

            if (counter == 3){
                state = State.OPEN;
                counter = 0;
                timer = System.currentTimeMillis();
            }

            if(state.equals(State.HALF_OPEN)){
                state = State.OPEN;
                timer = System.currentTimeMillis();
            }
        }

        return response;
    }

    @Override
    public void initialize(ServerConfiguration config) {
        weatherService.initialize(config);
    }

    @Override
    public void disconnect() {

    }

    @Override
    public ServerConfiguration getConfiguration() {
        return weatherService.getConfiguration();
    }

    public State getState() {
        return state;
    }

    //This method is created for testing
    public void setWeatherService(WeatherService weatherService){
        this.weatherService = weatherService;
    }

    public void setTimerThreshold(int newThreshold){
        timerThreshold = newThreshold;
    }
}
