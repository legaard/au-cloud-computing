package cloud.cave.service;

import cloud.cave.domain.Region;
import cloud.cave.ipc.CaveTimeOutException;
import cloud.cave.server.common.ServerConfiguration;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.logging.Logger;

public class CaveWeatherService implements WeatherService {
    private ServerConfiguration configuration;


    @Override
    public JSONObject requestWeather(String groupName, String playerID, Region region){
        JSONObject result = makeWeatherRequest(groupName, playerID, region);

        if (result == null || !result.get("errorMessage").toString().matches("OK")) {
            JSONObject failedResult = new JSONObject();
            failedResult.put("authenticated", "false");
            failedResult.put("errorMessage", "GroupName " + groupName + " or playerID "
                    + playerID + " is not authenticated");
            return failedResult;
        }

        return result;
    }

    private JSONObject makeWeatherRequest(String groupName, String playerID, Region region) throws CaveTimeOutException {
        JSONObject response = null;

        try {

            Unirest.setTimeouts(5000, 5000);

            HttpResponse<JsonNode> request = Unirest.get("http://{hostName}:{port}/cave/weather/api/v1/{groupName}/{playerID}/{region}")
                    .header("accept", "application/json")
                    .routeParam("hostName", configuration.get(0).getHostName())
                    .routeParam("port", String.valueOf(configuration.get(0).getPortNumber()))
                    .routeParam("groupName", groupName)
                    .routeParam("playerID", playerID)
                    .routeParam("region", convertRegion(region))
                    .asJson();
            String result = request.getBody().getObject().toString();

            try {
                JSONParser parser = new JSONParser();
                response = (JSONObject) parser.parse(result);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } catch (UnirestException e) {
            throw new CaveTimeOutException("*** Sorry - weather information is not available ***", e);
        }
        return response;
    }

    private String convertRegion(Region region) {
        String result = "";
        switch (region) {
            case AARHUS:
                result = "Arhus";
                break;
            case COPENHAGEN:
                result = "Copenhagen";
                break;
            case ODENSE:
                result = "Odense";
                break;
            case AALBORG:
                result = "Aalborg";
                break;
        }
        return result;
    }

    @Override
    public void initialize(ServerConfiguration config) {
        configuration = config;
    }

    @Override
    public void disconnect() {

    }

    @Override
    public ServerConfiguration getConfiguration() {
        return configuration;
    }
}
