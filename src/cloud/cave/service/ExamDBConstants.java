package cloud.cave.service;

public class ExamDBConstants {
    public static String DATABASE_NAME = "exam_cave";
    public static String ROOMS = "room_collection";
    public static String ROOM_DESCRIPTION = "room_description";
    public static String ROOM_POSITION = "pos";

    public static String PLAYERS = "player_collection";
    public static String PLAYER_ID = "player_id";
    public static String PLAYER_NAME = "player_name";
    public static String PLAYER_GROUP = "group_name";
    public static String PLAYER_REGION = "player_region";
    public static String PLAYER_SESSION = "session_id";
    public static String PLAYER_POSITION = "pos";
}
