package cloud.cave.service;

import cloud.cave.domain.Region;
import cloud.cave.ipc.CaveIPCException;
import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.server.common.SubscriptionRecord;
import cloud.cave.server.common.SubscriptionResult;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;

public class SubscriptionServiceAvailabilityDecorator implements SubscriptionService {

    CaveRegSubscriptionService caveRegSubscriptionService = new CaveRegSubscriptionService();
    MongoClient mongoClient;
    MongoCollection<Document> knownUsers;
    MongoCollection<Document> players;

    @Override
    public SubscriptionRecord lookup(String loginName, String password) {
        SubscriptionRecord subscriptionRecord = null;

        try {
             subscriptionRecord = caveRegSubscriptionService.lookup(loginName, password);

            if (subscriptionRecord.getErrorCode().equals(SubscriptionResult.LOGIN_NAME_HAS_VALID_SUBSCRIPTION)) {

                // Add to knownUsers collection if not already known
                if (knownUsers.find(eq(DBConstants.PLAYER_ID, subscriptionRecord.getPlayerID())).first() == null) {
                    Document newKnownUser = new Document(DBConstants.KNOWN_USERNAME, loginName)
                            .append(DBConstants.PLAYER_ID, subscriptionRecord.getPlayerID())
                            .append(DBConstants.KNOWN_USER_PASSWORD, password);
                    knownUsers.insertOne(newKnownUser);
                }
            }

        } catch (CaveIPCException e) {

            Document knownUser = knownUsers.find(eq(DBConstants.KNOWN_USERNAME, loginName)).first();

            if (knownUser == null) {
                throw e;
            } else {

                // Check that user password and username matches input
                if (knownUser.getString(DBConstants.KNOWN_USER_PASSWORD).equals(password)
                    && knownUser.getString(DBConstants.KNOWN_USERNAME).equals(loginName)) {

                    // Retrieve the information from players collection to create SubscriptionRecord
                    String playerID = knownUser.getString(DBConstants.PLAYER_ID);
                    Document playerRecord = (Document) players.find(eq(DBConstants.PLAYER_ID, playerID)).first().get(DBConstants.PLAYER_RECORD);

                    String playerName = playerRecord.getString(DBConstants.PLAYER_NAME);
                    String groupName = playerRecord.getString(DBConstants.PLAYER_GROUP);
                    Region region = MongoDBStorage.getRegion(playerRecord.getString(DBConstants.PLAYER_REGION));

                    subscriptionRecord = new SubscriptionRecord(playerID, playerName, groupName, region);
                }
            }
        }

        return subscriptionRecord;
    }

    @Override
    public void initialize(ServerConfiguration config) {

       //Initial connection to collection with known users
        mongoClient = new MongoClient(config.get(1).getHostName(), config.get(1).getPortNumber());
        MongoDatabase database = mongoClient.getDatabase(DBConstants.DATABASE_NAME);
        knownUsers = database.getCollection(DBConstants.KNOWN_USERS);
        players = database.getCollection(DBConstants.PLAYERS);

        caveRegSubscriptionService.initialize(config);
    }

    @Override
    public void disconnect() {
        caveRegSubscriptionService.disconnect();
    }

    @Override
    public ServerConfiguration getConfiguration() {
        return caveRegSubscriptionService.getConfiguration();
    }
}
