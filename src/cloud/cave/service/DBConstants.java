package cloud.cave.service;

public class DBConstants {
    public static String DATABASE_NAME = "skycave";
    public static String ROOMS = "rooms";
    public static String ROOM_DESCRIPTION = "desc";
    public static String ROOM_POSITION = "pos";


    public static String PLAYERS = "players";
    public static String PLAYER_RECORD = "playerRecord";
    public static String PLAYER_ID = "playerID";
    public static String PLAYER_NAME = "playerName";
    public static String PLAYER_GROUP = "playerGroup";
    public static String PLAYER_REGION = "playerRegion";
    public static String PLAYER_SESSION = "playerSession";
    public static String PLAYER_POSITION = "playerPosition";

    public static String KNOWN_USERS = "knownUsers";
    public static String KNOWN_USERNAME = "username";
    public static String KNOWN_USER_PASSWORD = "password";

}
