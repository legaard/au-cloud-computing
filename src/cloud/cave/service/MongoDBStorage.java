package cloud.cave.service;

import cloud.cave.domain.Direction;
import cloud.cave.domain.Region;
import cloud.cave.server.common.*;
import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import static com.mongodb.client.model.Filters.*;

import java.util.ArrayList;
import java.util.List;

public class MongoDBStorage implements CaveStorage {

    private ServerConfiguration serverConfiguration;
    private MongoClient mongoClient;
    private MongoCollection<Document> rooms;
    private MongoCollection<Document> players;
    private int longRoomDescriptionCounter = 0;

    @Override
    public RoomRecord getRoom(String positionString) {
        Document room = rooms.find(eq(DBConstants.ROOM_POSITION, positionString)).first();

        //If the room is null, then return null
        if (room == null){
            return null;
        }

        String description = room.getString(DBConstants.ROOM_DESCRIPTION);
        return new RoomRecord(description);
    }

    @Override
    public boolean addRoom(String positionString, RoomRecord description) {

        //If the room already exits return false
        if(rooms.find(eq(DBConstants.ROOM_POSITION, positionString)).first() != null){
            return false;
        }

        Document room = new Document(DBConstants.ROOM_POSITION, positionString)
                .append(DBConstants.ROOM_DESCRIPTION, description.description);
        rooms.insertOne(room);
        return true;
    }

    @Override
    public List<Direction> getSetOfExitsFromRoom(final String positionString) {

        final List<Direction> listOfExits = new ArrayList<>();
        Point3 pZero = Point3.parseString(positionString);
        Point3 p;

        //Iterate through all directions and find out what rooms are available
        for (final Direction exit : Direction.values()) {
            p = new Point3(pZero.x(), pZero.y(), pZero.z());
            p.translate(exit);
            String position = p.getPositionString();

            if(rooms.find(eq(DBConstants.ROOM_POSITION, position)).first() != null) {
                listOfExits.add(exit);
            }

        }
        return listOfExits;
    }

    @Override
    public PlayerRecord getPlayerByID(String playerID) {
        Document requestedPlayer = players.find(eq(DBConstants.PLAYER_ID, playerID)).first();

        if(requestedPlayer == null){
            return null;
        }

        return convertToPlayerRecord(requestedPlayer);
    }

    @Override
    public void updatePlayerRecord(PlayerRecord record) {

        //If the record does not exist a new one is created
        if (players.find(eq(DBConstants.PLAYER_ID, record.getPlayerID())).first() != null ){

            players.replaceOne(new Document(DBConstants.PLAYER_ID, record.getPlayerID()),
                    new Document(DBConstants.PLAYER_ID, record.getPlayerID())
                            .append(DBConstants.PLAYER_RECORD, new Document()
                                    .append(DBConstants.PLAYER_NAME, record.getPlayerName())
                                    .append(DBConstants.PLAYER_GROUP, record.getGroupName())
                                    .append(DBConstants.PLAYER_REGION, record.getRegion().toString())
                                    .append(DBConstants.PLAYER_SESSION, record.getSessionId())
                                    .append(DBConstants.PLAYER_POSITION, record.getPositionAsString())));

        }

        //Updates an existing room by replacing all values from the parameter
        else {

            Document player = new Document(DBConstants.PLAYER_ID, record.getPlayerID())
                    .append(DBConstants.PLAYER_RECORD, new Document()
                                    .append(DBConstants.PLAYER_NAME, record.getPlayerName())
                                    .append(DBConstants.PLAYER_GROUP, record.getGroupName())
                                    .append(DBConstants.PLAYER_REGION, record.getRegion().toString())
                                    .append(DBConstants.PLAYER_SESSION, record.getSessionId())
                                    .append(DBConstants.PLAYER_POSITION, record.getPositionAsString()));
            players.insertOne(player);
        }
    }

    public PlayerRecord convertToPlayerRecord(Document playerDocument){

        //Getting all the variables from the playerDocument
        String playerID = playerDocument.getString(DBConstants.PLAYER_ID);
        Document requestedRecordForPlayer = (Document) playerDocument.get(DBConstants.PLAYER_RECORD);
        String playerName = requestedRecordForPlayer.getString(DBConstants.PLAYER_NAME);
        String groupName = requestedRecordForPlayer.getString(DBConstants.PLAYER_GROUP);
        String stringRegion = requestedRecordForPlayer.getString(DBConstants.PLAYER_REGION);
        String position = requestedRecordForPlayer.getString(DBConstants.PLAYER_POSITION);
        String sessionID = requestedRecordForPlayer.getString(DBConstants.PLAYER_SESSION);
        Region playerRegion;

        playerRegion = getRegion(stringRegion);

        //Creating the SubscriptionRecord for the PlayerRecord
        SubscriptionRecord playerSubscription = new SubscriptionRecord(playerID, playerName ,groupName, playerRegion);

        return new PlayerRecord(playerSubscription, position, sessionID);
    }

    public static Region getRegion(String stringRegion) {

        Region playerRegion;
        switch (stringRegion) {
            case "AARHUS":
                playerRegion = Region.AARHUS;
                break;
            case "COPENHAGEN":
                playerRegion = Region.COPENHAGEN;
                break;
            case "ODENSE":
                playerRegion = Region.ODENSE;
                break;
            case "AALBORG":
                playerRegion = Region.AALBORG;
                break;
            case "NONE":
                playerRegion = Region.NONE;
                break;
            default:
                playerRegion = null;
        }
        return playerRegion;
    }

    @Override
    public List<PlayerRecord> computeListOfPlayersAt(String positionString) {
        final List<PlayerRecord> playerRecordList = new ArrayList<>();

        String combinedRecordAndPosition = DBConstants.PLAYER_RECORD + "." + DBConstants.PLAYER_POSITION;
        String combinedRecordAndSession = DBConstants.PLAYER_RECORD + "." + DBConstants.PLAYER_SESSION;

        FindIterable<Document> playersAtPosition;

        if (longRoomDescriptionCounter == 0) {
            playersAtPosition = players.find(and(eq(combinedRecordAndPosition, positionString), ne(combinedRecordAndSession, null))).limit(10);
        } else {
            playersAtPosition = players.find(and(eq(combinedRecordAndPosition, positionString), ne(combinedRecordAndSession, null))).skip((20*longRoomDescriptionCounter) - 10).limit(20);
        }

        //Iterate through all players at positionString
        playersAtPosition.forEach(new Block<Document>() {
            @Override
            public void apply(Document document) {
                playerRecordList.add(convertToPlayerRecord(document));
            }
        });

        // Only count up if the size is greater than 10
        if (playerRecordList.size() >= 10){
            longRoomDescriptionCounter++;
        }

        return playerRecordList;
    }

    @Override
    public int computeCountOfActivePlayers() {
        int numberOfActivePlayer = 0;

        FindIterable<Document> allPlayers = players.find();

        for (Document document : allPlayers) {

            //Check if the player is in the cave before incrementing the counter
            if (convertToPlayerRecord(document).isInCave()){
                numberOfActivePlayer++;
            }
        }

        return numberOfActivePlayer;
    }

    @Override
    public void resetBoundedLook() {
        longRoomDescriptionCounter = 0;
    }

    @Override
    public int getNumberOfRooms() {
        return (int) rooms.count();
    }

    @Override
    public void initialize(ServerConfiguration config) {
        serverConfiguration = config;
        List<ServerAddress> serverList = new ArrayList<>();

        for (int i = 0; i < serverConfiguration.size(); i++) {
            ServerAddress server = new ServerAddress(serverConfiguration.get(i).getHostName(), serverConfiguration.get(i).getPortNumber());
            serverList.add(server);
        }

        //Connecting to MongoDB
        mongoClient = new MongoClient(serverList);
        MongoDatabase database = mongoClient.getDatabase(DBConstants.DATABASE_NAME);

        //Creating the collections for the database (if they don't exist)
        rooms = database.getCollection(DBConstants.ROOMS);
        players = database.getCollection(DBConstants.PLAYERS);

        createFakeData();
    }

    @Override
    public void disconnect() {
        mongoClient.close();
    }

    @Override
    public ServerConfiguration getConfiguration() {
        return serverConfiguration;
    }

    private void createFakeData() {
        //create four rooms like in the fake storage.
        this.addRoom(new Point3(0, 0, 0).getPositionString(), new RoomRecord(
                "You are standing at the end of a road before a small brick building."));
        this.addRoom(new Point3(0, 1, 0).getPositionString(), new RoomRecord(
                "You are in open forest, with a deep valley to one side."));
        this.addRoom(new Point3(1, 0, 0).getPositionString(), new RoomRecord(
                "You are inside a building, a well house for a large spring."));
        this.addRoom(new Point3(-1, 0, 0).getPositionString(), new RoomRecord(
                "You have walked up a hill, still in the forest."));
        this.addRoom(new Point3(0, 0, 1).getPositionString(), new RoomRecord(
                "You are in the top of a tall tree, at the end of a road."));
    }
}
