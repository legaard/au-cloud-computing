package cloud.cave.extension;


import cloud.cave.ipc.Marshaling;
import cloud.cave.server.common.Command;
import org.json.simple.JSONObject;

public class CountRoomCommand extends AbstractCommand implements Command{

    @Override
    public JSONObject execute(String... parameters) {

        int numbOfRooms = storage.getNumberOfRooms();

        if (numbOfRooms == 0){
            return Marshaling.createInvalidReplyWithExplantion("false", "Apparently there is no room in the SkyCave.");
        }

        return Marshaling.createValidReplyWithReturnValue(String.valueOf(numbOfRooms));
    }
}
