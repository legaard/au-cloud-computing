package cloud.cave.extension;

import cloud.cave.ipc.Marshaling;
import cloud.cave.server.common.Command;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.simple.JSONObject;

public class QuoteCommand extends AbstractCommand implements Command{

    @Override
    public JSONObject execute(String... parameters) {
        String quote = "";
        String quoteNumber = parameters[0];

        try {
            HttpResponse<JsonNode> request = Unirest.get("http://quote:6745/albert/{quoteNumber}")
                    .header("accept", "application/json")
                    .routeParam("quoteNumber", quoteNumber)
                    .asJson();

            quote = request.getBody().getObject().get("quote").toString();

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return Marshaling.createValidReplyWithReturnValue(quote);
    }
}
