package cloud.cave.doubles;

import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.server.common.SubscriptionRecord;
import cloud.cave.server.common.SubscriptionResult;
import cloud.cave.service.CaveRegSubscriptionService;
import cloud.cave.service.SubscriptionService;

public class TestStubCaveRegSubscriptionService implements SubscriptionService{

    CaveRegSubscriptionService caveRegSubscriptionService = new CaveRegSubscriptionService();

    @Override
    public SubscriptionRecord lookup(String loginName, String password) {
        if(loginName.equals("20117411") && password.equals("1337boi")){
            return new SubscriptionRecord(SubscriptionResult.LOGIN_NAME_HAS_VALID_SUBSCRIPTION);
        }
        return new SubscriptionRecord(SubscriptionResult.LOGIN_NAME_OR_PASSWORD_IS_UNKNOWN);
    }

    @Override
    public void initialize(ServerConfiguration config) {
        caveRegSubscriptionService.initialize(config);
    }

    @Override
    public void disconnect() {
        caveRegSubscriptionService.disconnect();
    }

    @Override
    public ServerConfiguration getConfiguration() {
        return caveRegSubscriptionService.getConfiguration();
    }
}
