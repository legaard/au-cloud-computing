package cloud.cave.doubles;

import cloud.cave.domain.Region;
import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.service.CaveWeatherCircuitBreaker;
import cloud.cave.service.WeatherService;
import org.json.simple.JSONObject;

import javax.swing.*;

/**
 * Created by Lorentzen on 10/09/15.
 */
public class CaveWeatherServiceSaboteur implements WeatherService {

    private String hostName = "caveweather.baerbak.com";

    private CaveWeatherCircuitBreaker caveWeatherCircuitBreaker = new CaveWeatherCircuitBreaker();
    private ServerConfiguration timeoutConfig = new ServerConfiguration(hostName, 8184);
    private ServerConfiguration normalConfig = new ServerConfiguration(hostName, 8182);

    @Override
    public JSONObject requestWeather(String groupName, String playerID, Region region) {
        if (JOptionPane.showConfirmDialog(null, "Would you like to use the timeout server?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            caveWeatherCircuitBreaker.initialize(timeoutConfig);
            return caveWeatherCircuitBreaker.requestWeather(groupName, playerID, region);
        } else {
            caveWeatherCircuitBreaker.initialize(normalConfig);
            return caveWeatherCircuitBreaker.requestWeather(groupName, playerID, region);
        }
    }

    @Override
    public void initialize(ServerConfiguration config) {
        caveWeatherCircuitBreaker.initialize(config);
    }

    @Override
    public void disconnect() {

    }

    @Override
    public ServerConfiguration getConfiguration() {
        return caveWeatherCircuitBreaker.getConfiguration();
    }
}
