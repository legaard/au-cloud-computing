package cloud.cave.doubles;

import cloud.cave.config.CaveServerFactory;
import cloud.cave.ipc.Invoker;
import cloud.cave.ipc.Reactor;
import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.service.CaveRegSubscriptionService;
import cloud.cave.service.CaveStorage;
import cloud.cave.service.SubscriptionService;
import cloud.cave.service.WeatherService;

public class SubscriptionTestDoubleFactory implements CaveServerFactory {
    @Override
    public CaveStorage createCaveStorage() {
        CaveStorage storage = new FakeCaveStorage();
        storage.initialize(null);
        return storage;
    }

    @Override
    public SubscriptionService createSubscriptionServiceConnector() {
        SubscriptionService service = new TestStubCaveRegSubscriptionService();
        service.initialize(new ServerConfiguration("cavereg.baerbak.com", 4567));
        return service;
    }

    @Override
    public WeatherService createWeatherServiceConnector() {
        WeatherService weatherService = new TestStubWeatherService();
        weatherService.initialize(null);
        return weatherService;
    }

    @Override
    public Reactor createReactor(Invoker invoker) {
        return null;
    }
}
