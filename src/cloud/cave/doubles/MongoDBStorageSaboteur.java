package cloud.cave.doubles;

import cloud.cave.domain.Direction;
import cloud.cave.server.common.PlayerRecord;
import cloud.cave.server.common.RoomRecord;
import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.service.CaveStorage;
import cloud.cave.service.MongoDBStorage;
import com.mongodb.MongoQueryException;
import com.mongodb.MongoSocketClosedException;
import com.mongodb.MongoSocketReadException;
import com.mongodb.MongoTimeoutException;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.*;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

import java.io.IOException;
import java.util.List;

public class MongoDBStorageSaboteur implements CaveStorage{

    public enum MongoExceptionType{
        SOCKET_READ, SOCKET_CLOSED, QUERY, TIMEOUT
    }


    MongoDBStorage storage = new MongoDBStorage();

    MongoExceptionType exceptionType = null;

    @Override
    public RoomRecord getRoom(String positionString) {
        return storage.getRoom(positionString);
    }

    @Override
    public boolean addRoom(String positionString, RoomRecord description) {

        switch (exceptionType){
            case SOCKET_READ:
                throw new MongoSocketReadException("MongoSocketReadException", null);
            case SOCKET_CLOSED:
                throw new MongoSocketClosedException("MongoSocketClosedException", null);
            case QUERY:
                throw new MongoQueryException(null, 0, "MongoQueryException");
            case TIMEOUT:
                throw new MongoTimeoutException("MongoTimeoutException");
                default:
                    return storage.addRoom(positionString, description);
        }

    }

    @Override
    public List<Direction> getSetOfExitsFromRoom(String positionString) {
        return storage.getSetOfExitsFromRoom(positionString);
    }

    @Override
    public PlayerRecord getPlayerByID(String playerID) {
        return storage.getPlayerByID(playerID);
    }

    @Override
    public void updatePlayerRecord(PlayerRecord record) {
        storage.updatePlayerRecord(record);
    }

    @Override
    public List<PlayerRecord> computeListOfPlayersAt(String positionString) {
        return storage.computeListOfPlayersAt(positionString);
    }

    @Override
    public int computeCountOfActivePlayers() {
        return storage.computeCountOfActivePlayers();
    }

    @Override
    public void resetBoundedLook() {
        storage.resetBoundedLook();
    }

    @Override
    public int getNumberOfRooms() {
        return storage.getNumberOfRooms();
    }

    @Override
    public void initialize(ServerConfiguration config) {
        storage.initialize(config);
    }

    @Override
    public void disconnect() {
        storage.disconnect();
    }

    @Override
    public ServerConfiguration getConfiguration() {
        return null;
    }

    public void setExceptionType(MongoExceptionType exceptionType){
        this.exceptionType = exceptionType;
    }

}
