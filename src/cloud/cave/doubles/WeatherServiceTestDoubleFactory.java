package cloud.cave.doubles;

import cloud.cave.config.CaveServerFactory;
import cloud.cave.ipc.Invoker;
import cloud.cave.ipc.Reactor;
import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.service.*;

public class WeatherServiceTestDoubleFactory implements CaveServerFactory {
    @Override
    public CaveStorage createCaveStorage() {
        CaveStorage storage = new FakeCaveStorage();
        storage.initialize(null);
        return storage;
    }

    @Override
    public SubscriptionService createSubscriptionServiceConnector() {
        SubscriptionService service = new CaveRegSubscriptionService();
        service.initialize(new ServerConfiguration("cavereg.baerbak.com", 4567));
        return service;
    }

    @Override
    public WeatherService createWeatherServiceConnector() {
        WeatherService service = new CaveWeatherService();
        service.initialize(new ServerConfiguration("caveweather.baerbak.com", 8182));
        return service;
    }

    @Override
    public Reactor createReactor(Invoker invoker) {
        return null;
    }
}
