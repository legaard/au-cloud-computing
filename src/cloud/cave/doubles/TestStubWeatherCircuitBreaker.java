package cloud.cave.doubles;

import cloud.cave.domain.Region;
import cloud.cave.ipc.CaveTimeOutException;
import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.service.WeatherService;
import org.json.simple.JSONObject;

public class TestStubWeatherCircuitBreaker implements WeatherService{

    boolean shouldThrowException = false;

    @Override
    public JSONObject requestWeather(String groupName, String playerID, Region region) throws CaveTimeOutException{
        JSONObject reply = new JSONObject();

        if (shouldThrowException){
            throw new CaveTimeOutException("*** Sorry - weather information is not available ***",
                    new Exception("Test exception"));
        }

        reply.put("errorMessage", "OK");

        return reply;
    }

    @Override
    public void initialize(ServerConfiguration config) {
        //Do nothing
    }

    @Override
    public void disconnect() {
        //Do nothing
    }

    @Override
    public ServerConfiguration getConfiguration() {
        return null;
    }

    public void throwException(boolean bool){
        if(bool){
           shouldThrowException = true;
        } else {
            shouldThrowException = false;
        }
    }
}
