package cloud.cave.doubles;

import cloud.cave.server.common.ServerConfiguration;
import cloud.cave.service.CaveStorage;

public class StorageTestDoubleFactory extends AllTestDoubleFactory{

    MongoDBStorageSaboteur saboteur = new MongoDBStorageSaboteur();

    @Override
    public CaveStorage createCaveStorage() {
        saboteur.initialize(new ServerConfiguration("localhost", 12345));
        return saboteur;
    }

    public void setExceptionType(MongoDBStorageSaboteur.MongoExceptionType exceptionType){
        saboteur.setExceptionType(exceptionType);
    }
}
