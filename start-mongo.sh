#!/usr/bin/env bash

cd root/cave/

source setupLocalSocket.sh

# = Cave storage - overrides the EV from the above bash script
export SKYCAVE_CAVESTORAGE_IMPLEMENTATION=cloud.cave.service.MongoDBStorage
export SKYCAVE_DBSERVER=db0:27017

echo Daemon is now configured with MongoDB storage

ant daemon